package sojern.android.packageName.custom_views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import sojern.android.packageName.R;


/**
 * Created by anil.bisht on 28-03-2016.
 */
public class RotatingDotView extends LinearLayout {
    public RotatingDotView(Context context) {
        super(context);

        init(context);

    }

    public RotatingDotView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);

    }

    public RotatingDotView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);

    }
    private void init(Context context) {
        inflate(getContext(), R.layout.view_three_dot_progress, this);

        //View dotContainer = findViewById(R.id.container_dot);

        /*Animation rotateAnim = AnimationUtils.loadAnimation(context, R.anim.rotate_infinite);
        dotContainer.setAnimation(rotateAnim);*/
    }



}
