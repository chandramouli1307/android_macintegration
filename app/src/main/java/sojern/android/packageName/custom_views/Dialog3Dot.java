package sojern.android.packageName.custom_views;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

/**
 * Created by anil.bisht on 28-03-2016.
 */
public class Dialog3Dot {


    public static Dialog show(Context context){

        Dialog d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        d.setContentView(new RotatingDotView(context));

        d.show();

        return d;
    }


}
