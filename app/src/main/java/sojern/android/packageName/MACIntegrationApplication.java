package sojern.android.packageName;

import android.os.Build;

import com.tune.Tune;
import com.tune.ma.application.TuneApplication;

/**
 * Created by chandramoulilatchireddy on 10/05/17.
 */

public class MACIntegrationApplication extends TuneApplication {
    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize TMC
        Tune.init(this, "877", "8c14d6bbe466b65211e781d62e301eec");

       // Tune.getInstance().setDebugMode(true);
    }
}