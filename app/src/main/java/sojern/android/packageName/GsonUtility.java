package sojern.android.packageName;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Modifier;
import java.util.List;

/**
 * Created by anil.bisht on 25-04-2016.
 */
public class GsonUtility {


    private static Gson gsonObj;
    private static Gson gsonObjExcludeExpose;

    public static <T> List getListFromString(String response) {
        List<T> list = getInstance().fromJson(response, new TypeToken<List<T>>() {
        }.getType());

        return list;
    }

    public static String toString(Object modal) {

        try {
            String str = new Gson().toJson(modal);
            return str;
        } catch (Exception e) {
            return "";
        }
    }

    public static Gson getInstance() {
        if (gsonObj == null) {
            GsonBuilder builder = new GsonBuilder();
            builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
            //builder.excludeFieldsWithoutExposeAnnotation();
            gsonObj = builder.create();
        }

        return gsonObj;
    }

    public static Gson getInstanceWithoutExpose() {
        if (gsonObjExcludeExpose == null)
            gsonObjExcludeExpose = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gsonObjExcludeExpose;
    }

    public static <T> T fromStrToObj(String json, Class<T> classOfT) {

        try {

            return getInstance().fromJson(json, classOfT);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static <T> T fromStrToObjWithoutExpose(String json, Class<T> classOfT) {

        try {

            return getInstanceWithoutExpose().fromJson(json, classOfT);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String fromObjToStr(Object object) {

        try {

            return getInstance().toJson(object);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String fromObjToStrWithoutExpose(Object object) {

        try {

            return getInstanceWithoutExpose().toJson(object);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }
}
