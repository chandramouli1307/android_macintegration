package sojern.android.packageName;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import sojern.android.packageName.interfaces.CustomDialoInterface;

/**
 * Created by Sindhu on 4/28/2017.
 */

public class Utils {
    public static final String INTERNET_MODE_WIFI = "WIFI";
    public static final String INTERNET_MODE_MOBILE = "MOBILE";
    public static boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase(INTERNET_MODE_WIFI)) {
                if (ni.isConnected()) {
                    haveConnectedWifi = true;

                }
            } else if (ni.getTypeName().equalsIgnoreCase(INTERNET_MODE_MOBILE)) {
                if (ni.isConnected()) {
                    haveConnectedMobile = true;

                }
            }

        }
        return haveConnectedWifi || haveConnectedMobile;
    }


    public static void showCustomDialog(Context context, String title, String message, String negative, String positive, final CustomDialoInterface mCustomDialoInterface) {


        Activity instance = (Activity) context;
        Rect displayRectangle = new Rect();
        Window window = instance.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);


        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        View view = (View) LayoutInflater.from((context))
                .inflate(R.layout.dialog_yes_no, null);

        TextView mainMessage = (TextView)view.findViewById(R.id.main_message);
        TextView proceedButton = (TextView)view.findViewById( R.id.button_proceed);
        TextView cancelButton =  (TextView)view.findViewById( R.id.button_cancel);

        mainMessage.setText(message);
        proceedButton.setText(positive);
        cancelButton.setText(negative);

        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (mCustomDialoInterface != null) {
                    mCustomDialoInterface.clickonButton(true);
                }

            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (mCustomDialoInterface != null) {
                    mCustomDialoInterface.clickonButton(false);
                }

            }
        });
        dialog.show();

        dialog.setContentView(view);
        dialog.getWindow()
                .setLayout((int) (displayRectangle.width() * 0.84), (int) (displayRectangle.height() * 0.84));
        dialog.show();


    }
    public static void showSnackWithButton(Context context, String msg) {


        if (context instanceof Activity) {
            Snackbar snackbar = Snackbar
                    .make(((Activity) context).findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                        }
                    });

            // Changing message text color
            snackbar.setActionTextColor(Color.RED);

            // Changing action button text color
            View sbView = snackbar.getView();
            if (sbView != null) {
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_action);
                if (textView != null)
                    textView.setTextColor(Color.YELLOW);
            }

            snackbar.show();
        }


    }

    public static RecyclerView.LayoutManager setVerticalLayoutManager(Context context, RecyclerView mRecyclerView, RecyclerView.Adapter adp) {


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        if (adp != null)
            mRecyclerView.setAdapter(adp);

        return layoutManager;
    }

    public static  String ReadFromfile(String fileName, Context context) {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try {
            fIn = context.getResources().getAssets()
                    .open(fileName, Context.MODE_WORLD_READABLE);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null) {
                returnString.append(line);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            } catch (Exception e2) {
                e2.getMessage();
            }
        }
        return returnString.toString();
    }

}
