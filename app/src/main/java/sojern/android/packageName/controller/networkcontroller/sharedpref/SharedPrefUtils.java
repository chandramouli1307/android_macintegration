package sojern.android.packageName.controller.networkcontroller.sharedpref;

import android.content.Context;
import android.content.SharedPreferences;


/**
 * Created by anil.bisht on 06-04-2016.
 */
public class SharedPrefUtils extends ObscuredSharedPreferences {


    private static SharedPrefUtils mSharedPrefObj;

    public SharedPrefUtils(Context context) {
        super(context);
    }

    public static String getUserID(Context mContext) {
        return getInstance(mContext).getValue(KEY.USER_ID, "");
    }

    public static String getUserTokken(Context mContext) {
        return getInstance(mContext).getValue(KEY.USER_TOKEN, "");

    }

    public static String getUserName(Context mContext) {
        return getInstance(mContext).getValue(KEY.USER_NAME, "");
    }

    public static SharedPrefUtils getInstance(Context context) {

        if (mSharedPrefObj == null)
            mSharedPrefObj = new SharedPrefUtils(context);

        return mSharedPrefObj;
    }

    //Save Data delegates
    public void saveValue(String key, String value) {
        SharedPreferences.Editor editor = edit();
        editor.putString(key, value);
        editor.commit();

    }

    public void saveValue(String name, Boolean value) {
        SharedPreferences.Editor editor = edit();
        editor.putBoolean(name, value);
        editor.commit();
    }

    public void saveValue(String name, int value) {
        SharedPreferences.Editor editor = edit();
        editor.putInt(name, value);
        editor.commit();
    }

    public void saveValue(String name, float value) {
        SharedPreferences.Editor editor = edit();
        editor.putFloat(name, value);
        editor.commit();
    }

    public void saveValue(String name, long value) {
        SharedPreferences.Editor editor = edit();
        editor.putLong(name, value);
        editor.commit();
    }



    public int getValue(String name, int value) {

        return getInt(name, value);
    }

    public String getValue(String name, String value) {

        return getString(name, value);
    }


    //Get Values delegates

    public float getValue(String name, float value) {

        float fl = getFloat(name, value);
        return fl;
    }

    public boolean getValue(String name, Boolean value) {

        boolean fl = getBoolean(name, value);
        return fl;
    }

    public long getValue(String name, long value) {

        long fl = getLong(name, value);
        return fl;
    }







    public interface KEY {
        String USERPINCODE = "USERPINCODE";
        String SERVERUPDATEVERSION = "SERVERUPDATEVERSION";
        String UPDATETIME = "USERPINCODE";


        String USER_ID = "USERID";
        String REFRESH_NEEDED = "REFRESH_NEEDED";
        String USER_TOKEN = "userToken";
        String UUID = "MOBILEIME";
        String GCM_DEVICE_TOKEN = "DEVICE_TOKEN";
        String NETWORK_TYPE = "NETWORK_TYPE";
        String IS_GCM_TOKEN_GENERETED = "TOKENGENERATED";
        String DEVICE_ID = "DEVICEID";
        String SIGIN_IN = "SIGININ";
        String SKIP_USER = "SKIP_USER";
        String LOCATION_SELECTED = "LOCATION_SELECTED";
        String MY_CITY = "MYCITY";
        String MY_LOCATION= "MY_LOCATION";
        String MY_CITY_ID = "MYCITY_ID";
        String DIFFERANCE = "DIFFERANCE";
        String CATEGORYS = "CATEGORYS";
        String SELECTED_INTEREST = "SELECTED_INTEREST";
        String SELECTED_POLICY = "SELECTED_POLICY";
        String VIDEO_FIRST = "VIDEO_FIRST";
        String SELECTED_SHOP_SETIP = "SELECTED_SHOP_SETIP";
        String XMPP_PASSWORD = "XMPPPASSWORD";
        String SMACK_ACCOUNT = "SMACKACCOUNT";
        String USER_GENDER = "USER_GENDER";
        String USER_NAME = "USER_NAME";
        String USER_IMAGE = "USER_IMAGE";
        String IS_SELLER = "isSeller";
        String MOBILE_IME = "MOBILEIME";
        String MOBILE_NUMBER = "MOBILENUMBER";
        String EMAIL = "EMAIL";
        String chatCompulsoryVersion = "chatCompulsoryVersion";

        String USER_PROFILE_RESPONSE = "USERPROFILE";
        String CHAT_SYNC_LAST_TIME_USER_THREAD = "CHAT_SYNC_LAST_TIME_USER_THREAD";
        String IS_ADDING_PRODUCT_FIRST_TIME = "IS_ADDING_PRODUCT_FIRST_TIME";
    }


}
