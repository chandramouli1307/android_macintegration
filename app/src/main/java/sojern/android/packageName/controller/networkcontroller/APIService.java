package sojern.android.packageName.controller.networkcontroller;



import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import sojern.android.packageName.Constants;
import sojern.android.packageName.models.response.GetCategorysResponse;

/**
 * Created by anil.bisht on 22-03-2016.
 */
public interface APIService {


    @GET(Constants.BASE_URL + "serve_json")
    Call<GetCategorysResponse> getCategories(

    );


    @POST(Constants.BASE_URL + "api/mobile_event")
    Call<GetCategorysResponse> postData(

            @Body HashMap<String, Object> infoObj, @Header("Content-Type") String contentType
    );




}
