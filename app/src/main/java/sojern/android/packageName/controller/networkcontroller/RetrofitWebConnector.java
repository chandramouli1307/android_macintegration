package sojern.android.packageName.controller.networkcontroller;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import sojern.android.packageName.Constants;
import sojern.android.packageName.controller.networkcontroller.sharedpref.SharedPrefUtils;

/**
 * Created by anil.bisht on 22-03-2016.
 */
public class RetrofitWebConnector {


//    public static final String DUMMY_USER_ID = "236";


    public static Retrofit retrofit;



    private static Converter.Factory getResponseConverter() {
        GsonBuilder builder = new GsonBuilder();
        Gson gsonObj = builder.excludeFieldsWithoutExposeAnnotation().create();
        return GsonConverterFactory.create(gsonObj);
    }



    public static APIService getConnector(SharedPrefUtils sharedPrefUtils) {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        Interceptor interceptorHeader = getHeaderInterceptor(sharedPrefUtils);

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).addInterceptor(interceptorHeader).build();

        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(getResponseConverter())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(client)
                .build();

        APIService webServices = retrofit.create(APIService.class);
        return webServices;


    }



    @NonNull
    private static Interceptor getHeaderInterceptor(final SharedPrefUtils sharedPrefUtils) {
        return new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {

                Request.Builder requestBuilder = chain.request().newBuilder();
                requestBuilder.addHeader("Content-Type", "application/json");
                Request newRequest = requestBuilder.build();
                return chain.proceed(newRequest);
            }
        };
    }


}
