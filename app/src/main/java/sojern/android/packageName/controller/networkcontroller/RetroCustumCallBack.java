package sojern.android.packageName.controller.networkcontroller;

import android.content.Context;


import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sojern.android.packageName.R;
import sojern.android.packageName.Utils;
import sojern.android.packageName.controller.networkcontroller.sharedpref.SharedPrefUtils;

/**
 * Created by anil.bisht on 22-03-2016.
 */
public abstract class RetroCustumCallBack<T> extends RestServiceDialog implements Callback<T> {

    String apiName = "";
    String syncName = "";
    long time = 0;
    SharedPrefUtils mSharedPrefUtils;

    public RetroCustumCallBack(Context context, boolean showProgress, boolean isCancelAbleDialog, String apiName, String syncName) {
        super(context, isCancelAbleDialog);
        if (showProgress) {
            setDialog();
        }
        mSharedPrefUtils = SharedPrefUtils.getInstance(context);
        this.apiName = apiName;
        this.syncName = syncName;
        time = System.currentTimeMillis();

    }



    @Override
    public void onResponse(Call<T> call, Response<T> response) {

        stopDialog();

        boolean isSuccesFullCall = response.isSuccessful();
        Integer statusCode = response.code();
        String httpStatusMessage = response.message();

//        Log.d("LOG", response.body().toString());


        //Status code 200

        if (isSuccesFullCall && statusCode == 200) {

            T responseModal = response.body();

            if (responseModal != null ) {
                String networkName = "SERVER-RESPONSE";
                if (mSharedPrefUtils != null) {
                    networkName = mSharedPrefUtils.getValue(SharedPrefUtils.KEY.NETWORK_TYPE, "");
                }
                if (networkName.trim().length() <= 0) {
                    networkName = "SERVER-RESPONSE";
                }

                onSuccessStatus200(responseModal);
            } else {
                onSuccessStatus200Null();

            }

        } else {


            non200Process(statusCode);
            onSuccessStatusNon200(statusCode, httpStatusMessage);
        }

    }

    protected abstract void onSuccessStatus200(T responseModal);

    protected abstract void onSuccessStatus200Null();

    protected void onSuccessStatusNon200(int statusCode, String httpStatusMessage) {

    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        stopDialog();

        t.printStackTrace();

        errorMessagesToast(call, t);


    }

    public void non200Process(int statusCode) {


        switch (statusCode) {
            case 401:


                break;

            case 403:
              //  AppUtitlity.showcustomdialogWithNoButton(context.getResources().getString(R.string.opps), context.getResources().getString(R.string.you_are_blocked), context);

                break;
        }


    }


    private void errorMessagesToast(Call<T> call, Throwable t) {


        if (t != null) {

            if (t instanceof UnknownHostException) {
                Utils.showSnackWithButton(context, context.getResources().getString(R.string.network_server));

            } else if (t instanceof IOException) {
                Utils.showSnackWithButton(context, context.getResources().getString(R.string.network_server));

            } else if (t instanceof SocketTimeoutException) {
                Utils.showSnackWithButton(context, context.getResources().getString(R.string.time_out_error));

            } else {
                Utils.showSnackWithButton(context, t.getLocalizedMessage());

            }
        }

    }

}