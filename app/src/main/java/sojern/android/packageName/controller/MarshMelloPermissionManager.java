package sojern.android.packageName.controller;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import sojern.android.packageName.R;
import sojern.android.packageName.Utils;
import sojern.android.packageName.activitys.BaseActivity;
import sojern.android.packageName.interfaces.CustomDialoInterface;

/**
 * Created by anil.bisht on 30-06-2016.
 */
public class MarshMelloPermissionManager {


    public static final int REQUEST_PERMISSION_CAMERA = 10;
    public static final int REQUEST_PERMISSION_READ_WRITE = 11;
    private static final int REQUEST_PERMISSION_LOCATION = 12;

    public static boolean marshMellowPermissionCameraReadWrite(final BaseActivity context) {
        boolean isReadFromGalleryAllowed = checkPermissionAllowed(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        boolean isWriteToGalleryAllowed = checkPermissionAllowed(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        boolean isCameraAllowed = checkPermissionAllowed(context, Manifest.permission.CAMERA);
        if (!isReadFromGalleryAllowed || !isWriteToGalleryAllowed || !isCameraAllowed) {


            ActivityCompat.requestPermissions(context,
                    new String[]{
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA},
                    REQUEST_PERMISSION_CAMERA);

            return false;


        } else {

            return true;
        }

    }

    public static boolean marshMellowPermissionReadWrite(final BaseActivity coActivity) {

        boolean isReadFromGalleryAllowed = checkPermissionAllowed(coActivity, Manifest.permission.READ_EXTERNAL_STORAGE);
        boolean isWriteToGalleryAllowed = checkPermissionAllowed(coActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE);


        if (!isReadFromGalleryAllowed || !isWriteToGalleryAllowed) {

            ActivityCompat.requestPermissions(coActivity,
                    new String[]{
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION_READ_WRITE);
            return
                    false;
        } else

        {

            return
                    true;
        }

    }

    public static boolean marshMellowPermissionReadPhoneState(final BaseActivity coActivity) {

        boolean isReadFromGalleryAllowed = checkPermissionAllowed(coActivity, Manifest.permission.READ_PHONE_STATE);

        if (!isReadFromGalleryAllowed) {

            ActivityCompat.requestPermissions(coActivity,
                    new String[]{
                            Manifest.permission.READ_PHONE_STATE},
                    REQUEST_PERMISSION_READ_WRITE);
            return
                    false;
        } else

        {

            return
                    true;
        }

    }

    public static boolean checkAllPermissionInResult(final BaseActivity context, int requestCode, String[] permissions, int[] grantResults) {

        try {
            int count = 0;
            for (int resultInt : grantResults) {
                if (resultInt == PackageManager.PERMISSION_DENIED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(context, permissions[count])) {

                    } else {

                        gotoPermissionSetting(context);


                    }

                    return false;
                }

                count++;

            }
        } catch (Exception e) {

        }

        return true;

    }

    private static void gotoPermissionSetting(final Activity coActivity) {

        Utils.showCustomDialog(coActivity, null, coActivity.getString(R.string.go_to_permission), "CANCEL", "SETTINGS", new CustomDialoInterface() {
            @Override
            public void clickonButton(boolean ispPositive) {
                if (ispPositive) {
                    final Intent i = new Intent();
                    i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    i.addCategory(Intent.CATEGORY_DEFAULT);
                    i.setData(Uri.parse("package:" + coActivity.getPackageName()));
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    coActivity.startActivity(i);
                }
            }
        });
    }

    public static boolean checkPermissionAllowed(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission)
                == PackageManager.PERMISSION_GRANTED;
    }

}