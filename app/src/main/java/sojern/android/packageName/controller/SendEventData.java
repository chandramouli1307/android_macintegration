package sojern.android.packageName.controller;

import android.content.Context;

import com.tune.Tune;
import com.tune.TuneEvent;
import com.tune.TuneEventItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import sojern.android.packageName.Constants;
import sojern.android.packageName.controller.networkcontroller.RetroCustumCallBack;
import sojern.android.packageName.controller.networkcontroller.RetrofitWebConnector;
import sojern.android.packageName.controller.networkcontroller.sharedpref.SharedPrefUtils;
import sojern.android.packageName.models.EventItem;
import sojern.android.packageName.models.response.EventRequestModal;
import sojern.android.packageName.models.response.GetCategorysResponse;

/**
 * Created by chandramoulilatchireddy on 08/06/17.
 */

public class SendEventData {
    Context mContext;

    public SendEventData(Context context){
        this.mContext = context;


    }


    public void postData(ArrayList<EventItem> data, String event_uuid, String workflow_uuid, String event_name, String workflow_name,final List<TuneEventItem>  listEventItems,final TuneEvent tuneEvent) {



        ArrayList<EventRequestModal> mEventRequestModalList = new ArrayList<>();
        EventRequestModal mEventRequestModal = new EventRequestModal();
        mEventRequestModal.setEvent_name(event_name);
        mEventRequestModal.setEvent_uuid(event_uuid);
        mEventRequestModal.setOperating_system("Android");
        mEventRequestModal.setMobile_payload(data);
        mEventRequestModalList.add(mEventRequestModal);
        HashMap<String, Object> body= new HashMap<>();




        body.put("events", mEventRequestModalList);
        body.put("workflow_uuid", workflow_uuid);
        body.put("workflow_name", workflow_name);

        try {
            Call<GetCategorysResponse> callSendOtp = RetrofitWebConnector.getConnector(SharedPrefUtils.getInstance(mContext)).postData(body,"application/json");

            callSendOtp.enqueue(new RetroCustumCallBack<GetCategorysResponse>(mContext, false, false, "GEt", Constants.BLOCK) {
                @Override
                public void onFailure(Call<GetCategorysResponse> call, Throwable t) {
                    super.onFailure(call, t);

                }

                @Override
                protected void onSuccessStatus200(GetCategorysResponse responseModal) {

                }

                @Override
                protected void onSuccessStatus200Null(){
                }

                @Override
                protected void onSuccessStatusNon200(int statusCode, String httpStatusMessage) {
                    super.onSuccessStatusNon200(statusCode, httpStatusMessage);
                    if(statusCode==201){
                        Tune.getInstance().measureEvent(tuneEvent
                                .withEventItems(listEventItems));
                    }

                }
            });
        } catch (Exception e) {

        }
    }
}
