package sojern.android.packageName.controller.networkcontroller;

import android.app.Dialog;
import android.content.Context;
import sojern.android.packageName.custom_views.Dialog3Dot;


public class RestServiceDialog {


    public Context context;
//    public String title = DEF_title;
//    public static String DEF_title = "Loading...";
    private Dialog dialog;
    private boolean isCancelable = false;


    public RestServiceDialog(Context context, Boolean isCancelAbleDialog) {
        this.context = context;
//        DEF_title = AppUtitlity.getStringFromResource(context, R.string.msg_loading);

        if(isCancelAbleDialog!=null)
            isCancelable = isCancelAbleDialog;

    }


    public void setDialog() {

        try {

//            dialog = new MaterialDialog.Builder(context)
//                    .progress(true, 0)
//                    .backgroundColor(context.getResources().getColor(R.color.grey_line_dark))
//                    .widgetColor(context.getResources().getColor(android.R.color.white))
//                    .contentColor(context.getResources().getColor(R.color.colorAccent))
//                    .content(title).cancelable(false).show();

            dialog = Dialog3Dot.show(context);
            dialog.setCancelable(isCancelable);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopDialog() {

        try {

            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}