package sojern.android.packageName.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by chandramoulilatchireddy on 11/05/17.
 */

public class Event {

    @SerializedName("id")
    @Expose
    private String id="";

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("eventitems")
    @Expose
    private ArrayList<EventItem> eventitems;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<EventItem> getEventitems() {
        return eventitems;
    }

    public void setEventitems(ArrayList<EventItem> eventitems) {
        this.eventitems = eventitems;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
