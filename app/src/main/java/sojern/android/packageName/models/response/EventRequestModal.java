package sojern.android.packageName.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import sojern.android.packageName.models.EventItem;

/**
 * Created by chandramoulilatchireddy on 23/05/17.
 */

public class EventRequestModal {

    @SerializedName("event_uuid")
    @Expose
    private String event_uuid;

    @SerializedName("operating_system")
    @Expose
    private String operating_system;

    @SerializedName("event_name")
    @Expose
    private String event_name;

    @SerializedName("mobile_payload")
    @Expose
    private ArrayList <EventItem> mobile_payload;

    public String getEvent_uuid() {
        return event_uuid;
    }

    public void setEvent_uuid(String event_uuid) {
        this.event_uuid = event_uuid;
    }

    public String getOperating_system() {
        return operating_system;
    }

    public void setOperating_system(String operating_system) {
        this.operating_system = operating_system;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public ArrayList<EventItem> getMobile_payload() {
        return mobile_payload;
    }

    public void setMobile_payload(ArrayList<EventItem> mobile_payload) {
        this.mobile_payload = mobile_payload;
    }
}
