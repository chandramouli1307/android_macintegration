package sojern.android.packageName.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by chandramoulilatchireddy on 11/05/17.
 */

public class Category {

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("workflows")
    @Expose
    private ArrayList<Workflow> workflows;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Workflow> getWorkflows() {
        return workflows;
    }

    public void setWorkflows(ArrayList<Workflow> workflows) {
        this.workflows = workflows;
    }
}
