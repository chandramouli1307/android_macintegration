package sojern.android.packageName.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by chandramoulilatchireddy on 11/05/17.
 */

public class EventItem {

    @SerializedName("event_item_name")
    @Expose
    private String event_item_name;

    @SerializedName("attribute_sub1")
    @Expose
    private String attribute_sub1;


    @SerializedName("attribute_sub2")
    @Expose
    private String attribute_sub2;

    @SerializedName("attribute_sub3")
    @Expose
    private String attribute_sub3;

    @SerializedName("attribute_sub4")
    @Expose
    private String attribute_sub4;

    @SerializedName("attribute_sub5")
    @Expose
    private String attribute_sub5;

    public String getEvent_item_name() {
        return event_item_name;
    }

    public void setEvent_item_name(String event_item_name) {
        this.event_item_name = event_item_name;
    }

    public String getAttribute_sub1() {
        return attribute_sub1;
    }

    public void setAttribute_sub1(String attribute_sub1) {
        this.attribute_sub1 = attribute_sub1;
    }

    public String getAttribute_sub2() {
        return attribute_sub2;
    }

    public void setAttribute_sub2(String attribute_sub2) {
        this.attribute_sub2 = attribute_sub2;
    }

    public String getAttribute_sub3() {
        return attribute_sub3;
    }

    public void setAttribute_sub3(String attribute_sub3) {
        this.attribute_sub3 = attribute_sub3;
    }

    public String getAttribute_sub4() {
        return attribute_sub4;
    }

    public void setAttribute_sub4(String attribute_sub4) {
        this.attribute_sub4 = attribute_sub4;
    }

    public String getAttribute_sub5() {
        return attribute_sub5;
    }

    public void setAttribute_sub5(String attribute_sub5) {
        this.attribute_sub5 = attribute_sub5;
    }
}
