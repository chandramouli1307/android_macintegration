package sojern.android.packageName.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import sojern.android.packageName.models.Category;
import sojern.android.packageName.models.Workflow;

/**
 * Created by chandramoulilatchireddy on 11/05/17.
 */

public class GetCategorysResponse {

    @SerializedName("categories")
    @Expose
    private ArrayList<Category> categories;

    public ArrayList<Category> getCategory() {
        return categories;
    }

    public void setCategory(ArrayList<Category> category) {
        this.categories = category;
    }
}
