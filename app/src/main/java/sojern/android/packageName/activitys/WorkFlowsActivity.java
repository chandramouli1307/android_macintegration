package sojern.android.packageName.activitys;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.tune.Tune;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import sojern.android.packageName.Constants;
import sojern.android.packageName.GsonUtility;
import sojern.android.packageName.R;
import sojern.android.packageName.Utils;
import sojern.android.packageName.adapters.WorkflowListAdapter;
import sojern.android.packageName.controller.networkcontroller.RetroCustumCallBack;
import sojern.android.packageName.controller.networkcontroller.RetrofitWebConnector;
import sojern.android.packageName.controller.networkcontroller.sharedpref.SharedPrefUtils;
import sojern.android.packageName.custom_views.MarshMelloPermissionManager;
import sojern.android.packageName.models.Category;
import sojern.android.packageName.models.Workflow;
import sojern.android.packageName.models.response.GetCategorysResponse;

/**
 * Created by chandramoulilatchireddy on 31/05/17.
 */

public class WorkFlowsActivity extends BaseActivity {
    ArrayList<Category> mCategoryList;
    MaterialBetterSpinner category_spinner;
    RecyclerView work_flows_rv;
    WorkflowListAdapter mWorkflowListAdapter;

    public  ArrayList<Workflow> mWorkFlowList;
    Button go_button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_flows);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        work_flows_rv = (RecyclerView) findViewById(R.id.work_flows_rv);
        go_button = (Button)findViewById(R.id.go_button);
        category_spinner = (MaterialBetterSpinner) findViewById(R.id.category_spinner);
        String dummy_values[] = {"Category"};
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, dummy_values);

        category_spinner.setAdapter(arrayAdapter);


        if (MarshMelloPermissionManager.marshMellowPermissionReadPhoneState(this)) {
            sendToTune();
        }
        go_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickGo();
            }
        });
        Utils.setVerticalLayoutManager(this, work_flows_rv, null);
        getCategorys();


    }



    private void getCategorys() {




        try {
            Call<GetCategorysResponse> callSendOtp = RetrofitWebConnector.getConnector(SharedPrefUtils.getInstance(this)).getCategories();

            callSendOtp.enqueue(new RetroCustumCallBack<GetCategorysResponse>(this, true, false, "GEt", Constants.BLOCK) {
                @Override
                public void onFailure(Call<GetCategorysResponse> call, Throwable t) {
                    super.onFailure(call, t);

                }

                @Override
                protected void onSuccessStatus200(GetCategorysResponse responseModal) {
                    mCategoryList = responseModal.getCategory();
                    if(mCategoryList!=null&&mCategoryList.size()>0){
                        setCategorySpinner();
                    }

                }

                @Override
                protected void onSuccessStatus200Null(){
                }

                @Override
                protected void onSuccessStatusNon200(int statusCode, String httpStatusMessage) {
                    super.onSuccessStatusNon200(statusCode, httpStatusMessage);

                }
            });
        } catch (Exception e) {

        }
    }

    Workflow mWorkflow;

    private void setWorkFlowList(int pos){

        if(mCategoryList.get(pos).getWorkflows()!=null) {
            mWorkflow = new Workflow();
            mWorkflow.setName("All");
            mWorkFlowList = new ArrayList<>(mCategoryList.get(pos).getWorkflows());
            mWorkFlowList.add(0,mWorkflow);
            selectAllItems(false);
            mWorkflowListAdapter = new WorkflowListAdapter(this, this);
            work_flows_rv.setAdapter(mWorkflowListAdapter);
        }
    }

    private void setCategorySpinner(){

        category_spinner.setEnabled(true);
        String[] category_items = new String[ mCategoryList.size() ];

        for(int i=0;i<mCategoryList.size();i++){
            category_items[i] = mCategoryList.get(i).getName();
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, category_items);

        category_spinner.setAdapter(arrayAdapter);


        category_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setWorkFlowList(position);
            }
        });



    }
    private void sendToTune(){

        String deviceId = ((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
        Tune.getInstance().setDeviceId(deviceId);
        Tune.getInstance().setAndroidId(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));

        try {
            WifiManager wm = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            Tune.getInstance().setMacAddress(wm.getConnectionInfo().getMacAddress());
        } catch (NullPointerException e) {
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        boolean isAllowed = MarshMelloPermissionManager.checkAllPermissionInResult(this, requestCode, permissions, grantResults);


        if (isAllowed) {
            sendToTune();
        }


    }

    public void selectItem(int pos){
        if(mWorkFlowList!=null&&mWorkFlowList.size()>pos){
            if(mWorkFlowList.get(pos).isSelected()){
                mWorkFlowList.get(pos).setSelected(false);
                if(pos==0){
                    selectAllItems(false);
                }else{
                    mWorkFlowList.get(0).setSelected(false);

                }
            }else{
                mWorkFlowList.get(pos).setSelected(true);


                if(pos==0){
                    selectAllItems(true);
                }
            }
            mWorkflowListAdapter.notifyDataSetChanged();
        }
    }


    public void selectAllItems(boolean isSelected){

        if(mWorkFlowList!=null){
            for(int i=0;i<mWorkFlowList.size();i++){
                mWorkFlowList.get(i).setSelected(isSelected);
            }
        }

    }

    private void clickGo(){
        if(mWorkFlowList!=null&&mWorkFlowList.size()>0){
            List<Workflow> selectedList = new ArrayList<>();
            for(int i=1;i<mWorkFlowList.size();i++){
                if(mWorkFlowList.get(i).isSelected())
                    selectedList.add(mWorkFlowList.get(i));

            }

            if(selectedList.size()>0){
                Bundle b = new Bundle();
                b.putString("selected_list", GsonUtility.fromObjToStr(selectedList));
                Intent i = new Intent(this,WorkFlowStatusActivity.class);
                i.putExtras(b);
                startActivity(i);

            }else{
                Utils.showSnackWithButton(this,"Please select at least one workflow");
            }
        }else{
            Utils.showSnackWithButton(this,"Please select category");

        }
    }




}
