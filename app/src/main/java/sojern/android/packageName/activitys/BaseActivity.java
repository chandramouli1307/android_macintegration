package sojern.android.packageName.activitys;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.tune.ma.application.TuneActivity;


/**
 * Created by chandramouli.l on 11-06-2016.
 */
public abstract class BaseActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Build.VERSION.SDK_INT < 14) {
            TuneActivity.onStart(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT < 14) {
            TuneActivity.onResume(this);
        }
    }

    @Override
    public void onStop() {
        if (Build.VERSION.SDK_INT < 14) {
            TuneActivity.onStop(this);
        }
        super.onStop();
    }



















    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onPause() {
        super.onPause();


    }





}
