package sojern.android.packageName.activitys;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.tune.Tune;
import com.tune.TuneEvent;
import com.tune.TuneEventItem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import sojern.android.packageName.Constants;
import sojern.android.packageName.GsonUtility;
import sojern.android.packageName.R;
import sojern.android.packageName.Utils;
import sojern.android.packageName.adapters.WorkFlowStatusAdapter;
import sojern.android.packageName.adapters.WorkflowListAdapter;
import sojern.android.packageName.controller.SendEventData;
import sojern.android.packageName.controller.networkcontroller.RetroCustumCallBack;
import sojern.android.packageName.controller.networkcontroller.RetrofitWebConnector;
import sojern.android.packageName.controller.networkcontroller.sharedpref.SharedPrefUtils;
import sojern.android.packageName.interfaces.CustomDialoInterface;
import sojern.android.packageName.models.Event;
import sojern.android.packageName.models.EventItem;
import sojern.android.packageName.models.Workflow;
import sojern.android.packageName.models.response.EventRequestModal;
import sojern.android.packageName.models.response.GetCategorysResponse;

/**
 * Created by chandramoulilatchireddy on 31/05/17.
 */

public class WorkFlowStatusActivity extends BaseActivity {

     List<Workflow> mSelectedWorkFlows = new ArrayList<>();


    public List<Workflow> mCompletedWorkFlows = new ArrayList<>();


    WorkFlowStatusAdapter mWorkFlowStatusAdapter;
    RecyclerView work_flows_rv;

    TextView count_tv;
    public boolean isCompleted = true;
    boolean isOnScreen = true;

    int eValue = 0;

    int totalEventsCount = 0;
    int completedEventsCount = 0;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_flow_status);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        if(getIntent()!=null&&getIntent().getExtras()!=null){
            Bundle b = getIntent().getExtras();
            String value = b.getString("selected_list");
            if (value != null && value.length() > 0)
                mSelectedWorkFlows = GsonUtility.getInstance().fromJson(value, new TypeToken<List<Workflow>>() {
                }.getType());
            isCompleted = false;

            if(mSelectedWorkFlows!=null&&mSelectedWorkFlows.size()>0){
                for(int i=0;i<mSelectedWorkFlows.size();i++){
                    totalEventsCount = totalEventsCount+mSelectedWorkFlows.get(i).getEvents().size();
                }
            }
            work_flows_rv = (RecyclerView) findViewById(R.id.work_flows_rv);
            Utils.setVerticalLayoutManager(this, work_flows_rv, null);
            mWorkFlowStatusAdapter = new WorkFlowStatusAdapter(this, this);
            work_flows_rv.setAdapter(mWorkFlowStatusAdapter);
            count_tv = (TextView) findViewById(R.id.count_tv);
            count_tv.setText( "0 of " + totalEventsCount + " events completed");

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        workFlowUUid = UUID.randomUUID().toString();
                    } catch (Exception e) {

                    }
                    if(mSelectedWorkFlows!=null&&mSelectedWorkFlows.size()>0)
                      sendEvent(eValue);
                }
            }, 500);



        }
    }
    String workFlowUUid = "";
    String eventUUid = "";


    @Override
    public void onBackPressed() {
        if (isCompleted) {
            super.onBackPressed();
            return;
        }else{

            showDismissDialog();

        }
    }

    private void showDismissDialog(){
        Utils.showCustomDialog(this, "", "Some workflows are still on processing \n Are you sure you want to cancel", "Yes", "No", new CustomDialoInterface() {
            @Override
            public void clickonButton(boolean ispPositive) {
                if(ispPositive){

                }else{
                    finish();
                }

            }
        });
    }

    String eventnames[] = new String[]{
            "Custom - 1493619505068",
            "Custom - 1493623181390",
            "Custom - 1493623179676",
            "Custom - 1493703162407",
            "Custom - 1493703161340",
            "Custom - 1493703161923",
            "Custom - 1493619505937",
            "Custom - 1493619506572",
            TuneEvent.SHARE,
            TuneEvent.SEARCH,
            TuneEvent.SPENT_CREDITS,
            TuneEvent.ACHIEVEMENT_UNLOCKED,
            TuneEvent.NAME_SESSION,
            TuneEvent.ADDED_PAYMENT_INFO,
            TuneEvent.NAME_OPEN,
            TuneEvent.NAME_INSTALL,
            TuneEvent.NAME_UPDATE,
            "Custom - 1493623179676",
            "Custom - 1493703162407",
            "Custom - 1493703161340",
            "Custom - 1493703286527",
            "Custom - 1493703287043",
            "Custom - 1493703287395",
            "Custom - 1493703288232",
            "Custom - 1493703286527",
            "Custom - 1493703287043",
            "Custom - 1493703287395",
            "Custom - 1493703288232",
            "purchase1493370281424",
            "Custom - 1493623177890",
            "Custom - 1493623180637",
            "Custom - 1493618574000",
            "Custom - 1493618574903",
            "Custom - 1493703287815",

    };
    List<Event> eventsList;
    int eventIndex = 0;
    int workflowIndex=0;
    private void sendEvent(int x){


                    if(isOnScreen&&Utils.haveNetworkConnection(this)) {
                        workflowIndex =x;
                    eventsList = mSelectedWorkFlows.get(x).getEvents();
                        eventIndex = 0;

                        if(eventsList!=null){
                            sendEvents();
                        }



                }else{
                        isCompleted = true;
                        if(isOnScreen){
                            mWorkFlowStatusAdapter.notifyDataSetChanged();
                        }
                        findViewById(R.id.search_p).setVisibility(View.GONE);
                        Utils.showSnackWithButton(this,getResources().getString(R.string.no_internet_message));
                }








    }

    private void sendEvents(){


        if(eventsList.size()>eventIndex) {
            TuneEvent tuneEvent;
            if (workflowIndex % 2 == 0)
                tuneEvent = new TuneEvent(eventnames[eventIndex]);
            else
                tuneEvent = new TuneEvent(eventnames[eventIndex + 3]);

            ArrayList<EventItem> mEventitemsList = eventsList.get(eventIndex).getEventitems();
            JSONArray postData = new JSONArray();
            JSONObject object;


            try {
                eventUUid = UUID.randomUUID().toString();
            } catch (Exception e) {

            }
            List<TuneEventItem> listEventItems = new ArrayList<>();
            TuneEventItem mTuneEventItem;

            if (mEventitemsList != null && mEventitemsList.size() > 0) {

                for (int i = 0; i < mEventitemsList.size(); i++) {

                    try {
                        object = new JSONObject();
                        mTuneEventItem = new TuneEventItem(mEventitemsList.get(i).getEvent_item_name());
                        if (mEventitemsList.get(i).getAttribute_sub1() != null) {
                            mTuneEventItem.withAttribute1(mEventitemsList.get(i).getAttribute_sub1());
                            object.put("Attribute1", mEventitemsList.get(i).getAttribute_sub1());
                        }

                        if (mEventitemsList.get(i).getAttribute_sub2() != null) {
                            mTuneEventItem.withAttribute2(mEventitemsList.get(i).getAttribute_sub2());
                            object.put("Attribute2", mEventitemsList.get(i).getAttribute_sub2());
                        }

                        if (mEventitemsList.get(i).getAttribute_sub3() != null) {
                            mTuneEventItem.withAttribute3(mEventitemsList.get(i).getAttribute_sub3());
                            object.put("Attribute3", mEventitemsList.get(i).getAttribute_sub3());
                        }

                        if (mEventitemsList.get(i).getAttribute_sub4() != null) {
                            mTuneEventItem.withAttribute4(mEventitemsList.get(i).getAttribute_sub4());
                            object.put("Attribute4", mEventitemsList.get(i).getAttribute_sub4());
                        }

                        if (mEventitemsList.get(i).getAttribute_sub5() != null) {
                            mTuneEventItem.withAttribute5(mEventitemsList.get(i).getAttribute_sub5());
                            object.put("Attribute5", mEventitemsList.get(i).getAttribute_sub5());
                        }

                        listEventItems.add(mTuneEventItem);

                        postData.put(mEventitemsList.get(i));


                    } catch (Exception e) {

                    }
                }

            }

            listEventItems.add(new TuneEventItem("event_uuid")
                    .withAttribute1(eventUUid));
            listEventItems.add(new TuneEventItem("workflow_uuid")
                    .withAttribute1(workFlowUUid));



            eventsList.get(eventIndex).setId(eventUUid);

            new SendEventData(this).postData(eventsList.get(eventIndex).getEventitems(), eventUUid, workFlowUUid, eventsList.get(eventIndex).getName(), mSelectedWorkFlows.get(workflowIndex).getName(),listEventItems,tuneEvent);
            completedEventsCount = completedEventsCount + 1;
            count_tv.setText(completedEventsCount + " of " + totalEventsCount + " Events completed ( " + workFlowUUid.substring(workFlowUUid.trim().length() - 5).toUpperCase() + " )");

            if(eventIndex==0){
                Workflow workflow = new Workflow();
                workflow.setName(mSelectedWorkFlows.get(workflowIndex).getName());mSelectedWorkFlows.get(workflowIndex);
                ArrayList list_event = new ArrayList<Event>();
                list_event.add(mSelectedWorkFlows.get(workflowIndex).getEvents().get(eventIndex));
                workflow.setEvents(list_event);
                mCompletedWorkFlows.add(0,workflow);
                mWorkFlowStatusAdapter.notifyDataSetChanged();
            }else{
                ArrayList list_event = mCompletedWorkFlows.get(0).getEvents();
                list_event.add(mSelectedWorkFlows.get(workflowIndex).getEvents().get(eventIndex));
                mWorkFlowStatusAdapter.notifyDataSetChanged();

            }

                pauseSomeTimezforEvents();

        }else{
            mSelectedWorkFlows.get(workflowIndex).setWorkflow_id(workFlowUUid);
            mSelectedWorkFlows.get(workflowIndex).setSent(true);

            String saved_data = SharedPrefUtils.getInstance(this).getValue("saved_data", "");
            saved_data = workFlowUUid + "\n\n" + saved_data;
            SharedPrefUtils.getInstance(this).saveValue("saved_data", saved_data);
           // mCompletedWorkFlows.add(0,mSelectedWorkFlows.get(workflowIndex));
           // mWorkFlowStatusAdapter.notifyDataSetChanged();
            if (workflowIndex == mSelectedWorkFlows.size() - 1) {
                isCompleted = true;
                findViewById(R.id.search_p).setVisibility(View.GONE);

            } else {
                pauseSomeTime();

            }

        }
    }

    @Override
    protected void onDestroy() {
        isOnScreen=false;
        super.onDestroy();
    }


    private void pauseSomeTimezforEvents(){
        eventIndex = eventIndex+1;
        if(eventsList.size()>eventIndex) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    sendEvents();

                }
            }, 10000);
        }else{
            sendEvents();

        }
    }

    private void pauseSomeTime(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                eValue = eValue+1;
                if(mSelectedWorkFlows.size()>eValue)
                    sendEvent(eValue);

            }
        }, 70000);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            if(isCompleted)
                 finish();
            else
                showDismissDialog();
        }

        return super.onOptionsItemSelected(item);
    }

}
