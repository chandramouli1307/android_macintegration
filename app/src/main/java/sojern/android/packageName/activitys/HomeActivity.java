package sojern.android.packageName.activitys;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.tune.Tune;
import com.tune.TuneEvent;
import com.tune.TuneEventItem;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import sojern.android.packageName.Constants;
import sojern.android.packageName.GsonUtility;
import sojern.android.packageName.R;
import sojern.android.packageName.Utils;
import sojern.android.packageName.controller.networkcontroller.RetroCustumCallBack;
import sojern.android.packageName.controller.networkcontroller.RetrofitWebConnector;
import sojern.android.packageName.controller.networkcontroller.sharedpref.SharedPrefUtils;
import sojern.android.packageName.custom_views.Dialog3Dot;
import sojern.android.packageName.custom_views.MarshMelloPermissionManager;
import sojern.android.packageName.models.Category;
import sojern.android.packageName.models.EventItem;
import sojern.android.packageName.models.Event;
import sojern.android.packageName.models.Workflow;
import sojern.android.packageName.models.response.EventRequestModal;
import sojern.android.packageName.models.response.GetCategorysResponse;

public class HomeActivity extends BaseActivity {

    ArrayList<Category> mCategoryList;
    private Dialog dialog;
    int categoryIndex = -1;
    int workflowIndex = -1;
    int testCaseIndex = -1;
    MaterialBetterSpinner workflow_spinner;
    MaterialBetterSpinner test_cases_spinner;
    MaterialBetterSpinner category_spinner;
    Button go_button;
    Button all_button;

    TextView workflow_id_tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (MarshMelloPermissionManager.marshMellowPermissionReadPhoneState(this)) {
            sendToTune();
        }

        String response = Utils.ReadFromfile("sample.txt",this);



        workflow_id_tv = (TextView) findViewById(R.id.workflow_id_tv);

        go_button = (Button)findViewById(R.id.go_button);
        all_button = (Button)findViewById(R.id.all_button);

        category_spinner = (MaterialBetterSpinner) findViewById(R.id.category_spinner);
        workflow_spinner = (MaterialBetterSpinner) findViewById(R.id.workflow_spinner);
        test_cases_spinner = (MaterialBetterSpinner) findViewById(R.id.testcases_spinner);

        test_cases_spinner.setEnabled(false);
        workflow_spinner.setEnabled(false);


        String dummy_values[] = {"Category"};
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, dummy_values);

        category_spinner.setAdapter(arrayAdapter);


        dummy_values = new String[] {"Workflow"};
         arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, dummy_values);

        workflow_spinner.setAdapter(arrayAdapter);

        dummy_values = new String[] {"TestCases"};
        arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, dummy_values);

        test_cases_spinner.setAdapter(arrayAdapter);



        go_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(categoryIndex>-1&&workflowIndex>-1){


                //sendEvent();
                    List<Workflow> selectedList = new ArrayList<>();

                    if(categoryIndex>-1&&workflowIndex>-1) {
                        selectedList.add(mCategoryList.get(categoryIndex).getWorkflows().get(workflowIndex));


                        Bundle b = new Bundle();
                        b.putString("selected_list", GsonUtility.fromObjToStr(selectedList));
                        Intent i = new Intent(HomeActivity.this, WorkFlowStatusActivity.class);
                        i.putExtras(b);
                        startActivity(i);
                    }
                }
            }
        });

        go_button.setEnabled(false);
        getCategorys();

        String saved_data = SharedPrefUtils.getInstance(this).getValue("saved_data","");
        workflow_id_tv.setText(saved_data);

        all_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mCategoryList!=null&&mCategoryList.size()>0) {

                    List<Workflow> selectedList = new ArrayList<>();

                    for(int i=0;i<mCategoryList.size();i++){
                        if(mCategoryList.get(i).getWorkflows()!=null)
                        selectedList.addAll(mCategoryList.get(i).getWorkflows());
                    }

                    Bundle b = new Bundle();
                    b.putString("selected_list", GsonUtility.fromObjToStr(selectedList));
                    Intent i = new Intent(HomeActivity.this, WorkFlowStatusActivity.class);
                    i.putExtras(b);
                    startActivity(i);
                }

            }
        });





    }

    private void setCategorySpinner(){

        category_spinner.setEnabled(true);
        String[] category_items = new String[ mCategoryList.size() ];

        for(int i=0;i<mCategoryList.size();i++){
            category_items[i] = mCategoryList.get(i).getName();
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, category_items);

        category_spinner.setAdapter(arrayAdapter);


        category_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                categoryIndex=  position;
                workflowIndex = -1;
                testCaseIndex = -1;
                workflow_spinner.setEnabled(false);
                test_cases_spinner.setEnabled(false);
                go_button.setEnabled(false);
                setWorkflowSpinner();
            }
        });



    }


    private void setWorkflowSpinner(){

        ArrayList<Workflow> workflowList = mCategoryList.get(categoryIndex).getWorkflows();

        if(workflowList!=null&&workflowList.size()>0) {

            workflow_spinner.setEnabled(true);
            String[] work_flow_items = new String[workflowList.size()];

            for (int i = 0; i < workflowList.size(); i++) {
                work_flow_items[i] = workflowList.get(i).getName();
            }

            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_dropdown_item_1line, work_flow_items);

            workflow_spinner.setAdapter(arrayAdapter);

            workflow_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    workflowIndex = position;
                   // testCaseIndex = -1;
                    //test_cases_spinner.setEnabled(false);
                    go_button.setEnabled(true);
                    //setTestCasesSpinner();
                }
            });




        }
    }

    private void setTestCasesSpinner(){

        ArrayList<Event> testCasesList = mCategoryList.get(categoryIndex).getWorkflows().get(workflowIndex).getEvents();

        if(testCasesList!=null&&testCasesList.size()>0) {

            test_cases_spinner.setEnabled(true);
            String[] test_cases_items = new String[testCasesList.size()];

            for (int i = 0; i < testCasesList.size(); i++) {
                test_cases_items[i] = testCasesList.get(i).getName();
            }

            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_dropdown_item_1line, test_cases_items);

            test_cases_spinner.setAdapter(arrayAdapter);



            test_cases_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    testCaseIndex = position;
                    go_button.setEnabled(true);
                }
            });


        }
    }

    private void sendToTune(){

        String deviceId = ((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
        Tune.getInstance().setDeviceId(deviceId);
        Tune.getInstance().setAndroidId(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));

        try {
            WifiManager wm = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            Tune.getInstance().setMacAddress(wm.getConnectionInfo().getMacAddress());
        } catch (NullPointerException e) {
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        boolean isAllowed = MarshMelloPermissionManager.checkAllPermissionInResult(this, requestCode, permissions, grantResults);


        if (isAllowed) {
            sendToTune();
        }


    }





    String eventnames[] = new String[]{

            "Custom - 1493623177890",
            "Custom - 1493623180637",
            "Custom - 1493618574000",

            "Custom - 1493618574903",
            "Custom - 1493619505068",
            "Custom - 1493623181390",

            "Custom - 1493623179676",
            "Custom - 1493703286527",
            "Custom - 1493703287043",
            "Custom - 1493703287395",
            "Custom - 1493703288232",
            "purchase1493370281424",
            "Custom - 1493703162407",
            "Custom - 1493703161340",
            "Custom - 1493703161923",
            "Custom - 1493619505937",
            "Custom - 1493619506572",



            "Custom - 1493703287815",

            };


    private void sendEvent(){

        if(categoryIndex>-1&&workflowIndex>-1){

            setDialog();

            List<Event> eventsList = mCategoryList.get(categoryIndex).getWorkflows().get(workflowIndex).getEvents();
            String workFlowUUid = "";
            String eventUUid = "";
            try {
                workFlowUUid = UUID.randomUUID().toString();

            } catch (Exception e) {

            }
            for(int j=0;j<eventsList.size();j++) {

                testCaseIndex = j;
                TuneEvent tuneEvent = new TuneEvent(eventnames[j]);

                ArrayList<EventItem> mEventitemsList = mCategoryList.get(categoryIndex).getWorkflows().get(workflowIndex).getEvents().get(testCaseIndex).getEventitems();
                JSONArray postData = new JSONArray();
                JSONObject object;


                try {
                    eventUUid = UUID.randomUUID().toString();
                } catch (Exception e) {

                }
                List<TuneEventItem> listEventItems = new ArrayList<>();
                TuneEventItem mTuneEventItem;

                if (mEventitemsList != null && mEventitemsList.size() > 0) {

                    for (int i = 0; i < mEventitemsList.size(); i++) {

                        try {
                            object = new JSONObject();
                            mTuneEventItem = new TuneEventItem(mEventitemsList.get(i).getEvent_item_name());
                            if(mEventitemsList.get(i).getAttribute_sub1()!=null) {
                                mTuneEventItem.withAttribute1(mEventitemsList.get(i).getAttribute_sub1());
                                object.put("Attribute1",mEventitemsList.get(i).getAttribute_sub1());
                            }

                            if(mEventitemsList.get(i).getAttribute_sub2()!=null) {
                                mTuneEventItem.withAttribute2(mEventitemsList.get(i).getAttribute_sub2());
                                object.put("Attribute2",mEventitemsList.get(i).getAttribute_sub2());
                            }

                            if(mEventitemsList.get(i).getAttribute_sub3()!=null) {
                                mTuneEventItem.withAttribute3(mEventitemsList.get(i).getAttribute_sub3());
                                object.put("Attribute3",mEventitemsList.get(i).getAttribute_sub3());
                            }

                            if(mEventitemsList.get(i).getAttribute_sub4()!=null) {
                                mTuneEventItem.withAttribute4(mEventitemsList.get(i).getAttribute_sub4());
                                object.put("Attribute4",mEventitemsList.get(i).getAttribute_sub4());
                            }

                            if(mEventitemsList.get(i).getAttribute_sub5()!=null) {
                                mTuneEventItem.withAttribute5(mEventitemsList.get(i).getAttribute_sub5());
                                object.put("Attribute5",mEventitemsList.get(i).getAttribute_sub5());
                            }

                            listEventItems.add(mTuneEventItem);

                            postData.put(mEventitemsList.get(i));



                        } catch (Exception e) {

                        }
                    }

                }

                listEventItems.add(new TuneEventItem("event_uuid")
                        .withAttribute1(eventUUid));
                listEventItems.add(new TuneEventItem("workflow_uuid")
                        .withAttribute1(workFlowUUid));



                Tune.getInstance().measureEvent(tuneEvent
                        .withEventItems(listEventItems));



                postData(eventsList.get(j).getEventitems(), eventUUid, workFlowUUid,eventsList.get(j).getName(),mCategoryList.get(categoryIndex).getWorkflows().get(workflowIndex).getName());

                try{
                    Thread.sleep(500);
                }catch (Exception e){

                }



            }

            String saved_data = SharedPrefUtils.getInstance(this).getValue("saved_data","");
            saved_data = workFlowUUid+"\n\n"+saved_data;
            SharedPrefUtils.getInstance(this).saveValue("saved_data",saved_data);
            workflow_id_tv.setText(saved_data);

            }

            stopDialog();


    }


    private void getCategorys() {




        try {
            Call<GetCategorysResponse> callSendOtp = RetrofitWebConnector.getConnector(SharedPrefUtils.getInstance(this)).getCategories();

            callSendOtp.enqueue(new RetroCustumCallBack<GetCategorysResponse>(this, true, false, "GEt", Constants.BLOCK) {
                @Override
                public void onFailure(Call<GetCategorysResponse> call, Throwable t) {
                    super.onFailure(call, t);

                }

                @Override
                protected void onSuccessStatus200(GetCategorysResponse responseModal) {
                    mCategoryList = responseModal.getCategory();
                    if(mCategoryList!=null&&mCategoryList.size()>0){
                        setCategorySpinner();
                    }

                }

                @Override
                protected void onSuccessStatus200Null(){
                }

                @Override
                protected void onSuccessStatusNon200(int statusCode, String httpStatusMessage) {
                    super.onSuccessStatusNon200(statusCode, httpStatusMessage);

                }
            });
        } catch (Exception e) {

        }
    }


    private void postData(ArrayList<EventItem> data,String event_uuid,String workflow_uuid,String event_name,String workflow_name) {



        ArrayList<EventRequestModal> mEventRequestModalList = new ArrayList<>();
        EventRequestModal mEventRequestModal = new EventRequestModal();
        mEventRequestModal.setEvent_name(event_name);
        mEventRequestModal.setEvent_uuid(event_uuid);
        mEventRequestModal.setOperating_system("Android");
        mEventRequestModal.setMobile_payload(data);
        mEventRequestModalList.add(mEventRequestModal);
        HashMap<String, Object> body= new HashMap<>();


        body.put("events", mEventRequestModalList);
        body.put("workflow_uuid", workflow_uuid);
        body.put("workflow_name", workflow_name);

        try {
            Call<GetCategorysResponse> callSendOtp = RetrofitWebConnector.getConnector(SharedPrefUtils.getInstance(this)).postData(body,"application/json");

            callSendOtp.enqueue(new RetroCustumCallBack<GetCategorysResponse>(this, false, false, "GEt", Constants.BLOCK) {
                @Override
                public void onFailure(Call<GetCategorysResponse> call, Throwable t) {
                    super.onFailure(call, t);

                }

                @Override
                protected void onSuccessStatus200(GetCategorysResponse responseModal) {
                    mCategoryList = responseModal.getCategory();
                    if(mCategoryList!=null&&mCategoryList.size()>0){
                        setCategorySpinner();
                    }

                }

                @Override
                protected void onSuccessStatus200Null(){
                }

                @Override
                protected void onSuccessStatusNon200(int statusCode, String httpStatusMessage) {
                    super.onSuccessStatusNon200(statusCode, httpStatusMessage);

                }
            });
        } catch (Exception e) {

        }
    }

    public void setDialog() {

        try {

            dialog = Dialog3Dot.show(this);
            dialog.setCancelable(false);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopDialog() {

        try {

            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
