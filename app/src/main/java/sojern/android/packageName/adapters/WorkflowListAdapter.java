package sojern.android.packageName.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import sojern.android.packageName.R;
import sojern.android.packageName.activitys.WorkFlowsActivity;
import sojern.android.packageName.models.Workflow;

/**
 * Created by chandramoulilatchireddy on 31/05/17.
 */

public class WorkflowListAdapter extends RecyclerView.Adapter<WorkflowListAdapter.SimpleViewHolder> {


    WorkFlowsActivity instance;
    Context mContext;

    Workflow mWorkflow;


    public WorkflowListAdapter(Context context, WorkFlowsActivity instance) {
        mContext = context;
        this.instance = instance;


    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_work_flow, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, final int position) {


        mWorkflow = instance.mWorkFlowList.get(position);

        if(position==0){
            holder.header_layout.setVisibility(View.VISIBLE);
            holder.line_view.setVisibility(View.VISIBLE);
            holder.name_tv.setTypeface(null, Typeface.BOLD);

        }else{
            holder.header_layout.setVisibility(View.GONE);
            holder.line_view.setVisibility(View.GONE);
            holder.name_tv.setTypeface(null, Typeface.NORMAL);
        }
        holder.name_tv.setText(mWorkflow.getName());
        holder.select_cb.setChecked(mWorkflow.isSelected());
        holder.top_layout.setTag(position);
        holder.select_cb.setTag(position);
        holder.select_cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (int)v.getTag();
                instance.selectItem(pos);
            }
        });

        holder.top_layout.setTag(position);
        holder.top_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (int)v.getTag();
                instance.selectItem(pos);
            }
        });



    }

    @Override
    public int getItemCount() {
        return instance.mWorkFlowList.size();
    }



    public static class SimpleViewHolder extends RecyclerView.ViewHolder {


        @Bind(R.id.top_layout)
        LinearLayout top_layout;

        @Bind(R.id.name_tv)
        TextView name_tv;

        @Bind(R.id.select_cb)
        CheckBox select_cb;

        @Bind(R.id.header_layout)
        LinearLayout header_layout;

        @Bind(R.id.line_view)
        View line_view;


        public SimpleViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}

