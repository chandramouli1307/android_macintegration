package sojern.android.packageName.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import sojern.android.packageName.R;
import sojern.android.packageName.activitys.WorkFlowStatusActivity;
import sojern.android.packageName.activitys.WorkFlowsActivity;
import sojern.android.packageName.models.Event;
import sojern.android.packageName.models.Workflow;

/**
 * Created by chandramoulilatchireddy on 05/06/17.
 */

public class EventsListAdapter extends RecyclerView.Adapter<EventsListAdapter.SimpleViewHolder> {


    Context mContext;

    Event mEvent;

    List<Event> mEventList;


    public EventsListAdapter(Context context, List<Event> list) {
        mContext = context;
        this.mEventList = list;


    }

    @Override
    public EventsListAdapter.SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event, parent, false);
        return new EventsListAdapter.SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventsListAdapter.SimpleViewHolder holder, final int position) {


        mEvent = mEventList.get(position);

        holder.name_tv.setText(mEvent.getName());

        if(mEvent.getId().length()>5)
            holder.name_tv.setText(mEvent.getName() + "  (" + mEvent.getId().toUpperCase().substring(mEvent.getId().trim().length()-5)+")");


        holder.status_iv.setImageResource(R.drawable.ic_action_check_circle);
        holder.status_iv.setVisibility(View.VISIBLE);



    }

    @Override
    public int getItemCount() {
        return mEventList.size();
    }



    public static class SimpleViewHolder extends RecyclerView.ViewHolder {




        @Bind(R.id.name_tv)
        TextView name_tv;



        @Bind(R.id.status_iv)
        ImageView status_iv;



        public SimpleViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}

