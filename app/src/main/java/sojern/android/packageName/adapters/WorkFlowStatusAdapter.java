package sojern.android.packageName.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import sojern.android.packageName.R;
import sojern.android.packageName.Utils;
import sojern.android.packageName.activitys.WorkFlowStatusActivity;
import sojern.android.packageName.activitys.WorkFlowsActivity;
import sojern.android.packageName.models.Workflow;

/**
 * Created by chandramoulilatchireddy on 31/05/17.
 */

public class WorkFlowStatusAdapter extends RecyclerView.Adapter<WorkFlowStatusAdapter.SimpleViewHolder> {


    WorkFlowStatusActivity instance;
    Context mContext;

    Workflow mWorkflow;


    public WorkFlowStatusAdapter(Context context, WorkFlowStatusActivity instance) {
        mContext = context;
        this.instance = instance;


    }

    @Override
    public WorkFlowStatusAdapter.SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_work_flow_status, parent, false);
        return new WorkFlowStatusAdapter.SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(WorkFlowStatusAdapter.SimpleViewHolder holder, final int position) {


        mWorkflow = instance.mCompletedWorkFlows.get(position);


        if(mWorkflow.getWorkflow_id()!=null&&mWorkflow.getWorkflow_id().trim().length()>0) {
            holder.name_tv.setText(mWorkflow.getName().toUpperCase());

           // if(mWorkflow.getWorkflow_id().length()>0)
            //    holder.name_tv.setText(mWorkflow.getName() + " (" + mWorkflow.getWorkflow_id().substring(mWorkflow.getWorkflow_id().trim().length()-5)+")");

        }
        else
            holder.name_tv.setText(mWorkflow.getName().toUpperCase());

            holder.status_iv.setImageResource(R.drawable.ic_action_check_circle);
            holder.status_iv.setVisibility(View.VISIBLE);
            holder.event_list_rv.setVisibility(View.VISIBLE);
            Utils.setVerticalLayoutManager(mContext, holder.event_list_rv, new EventsListAdapter(mContext,mWorkflow.getEvents()));

        holder.status_iv.setVisibility(View.GONE);



    }

    @Override
    public int getItemCount() {
        return instance.mCompletedWorkFlows.size();
    }



    public static class SimpleViewHolder extends RecyclerView.ViewHolder {


        @Bind(R.id.top_layout)
        LinearLayout top_layout;

        @Bind(R.id.name_tv)
        TextView name_tv;



        @Bind(R.id.status_iv)
        ImageView status_iv;

        @Bind(R.id.line_view)
        View line_view;

        @Bind(R.id.event_list_rv)
        RecyclerView event_list_rv;




        public SimpleViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}

